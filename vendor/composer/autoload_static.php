<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInited47f589aa2a9a2c1c5dce02fd306d3e
{
    public static $files = array (
        '320cde22f66dd4f5d3fd621d3e88b98f' => __DIR__ . '/..' . '/symfony/polyfill-ctype/bootstrap.php',
    );

    public static $prefixLengthsPsr4 = array (
        'S' => 
        array (
            'Symfony\\Polyfill\\Ctype\\' => 23,
        ),
        'F' => 
        array (
            'Framework\\' => 10,
        ),
        'D' => 
        array (
            'Dotenv\\' => 7,
        ),
        'A' => 
        array (
            'App\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Symfony\\Polyfill\\Ctype\\' => 
        array (
            0 => __DIR__ . '/..' . '/symfony/polyfill-ctype',
        ),
        'Framework\\' => 
        array (
            0 => __DIR__ . '/../..' . '/framework',
        ),
        'Dotenv\\' => 
        array (
            0 => __DIR__ . '/..' . '/vlucas/phpdotenv/src',
        ),
        'App\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app',
        ),
    );

    public static $classMap = array (
        'Dotenv\\Dotenv' => __DIR__ . '/..' . '/vlucas/phpdotenv/src/Dotenv.php',
        'Dotenv\\Exception\\ExceptionInterface' => __DIR__ . '/..' . '/vlucas/phpdotenv/src/Exception/ExceptionInterface.php',
        'Dotenv\\Exception\\InvalidCallbackException' => __DIR__ . '/..' . '/vlucas/phpdotenv/src/Exception/InvalidCallbackException.php',
        'Dotenv\\Exception\\InvalidFileException' => __DIR__ . '/..' . '/vlucas/phpdotenv/src/Exception/InvalidFileException.php',
        'Dotenv\\Exception\\InvalidPathException' => __DIR__ . '/..' . '/vlucas/phpdotenv/src/Exception/InvalidPathException.php',
        'Dotenv\\Exception\\ValidationException' => __DIR__ . '/..' . '/vlucas/phpdotenv/src/Exception/ValidationException.php',
        'Dotenv\\Loader' => __DIR__ . '/..' . '/vlucas/phpdotenv/src/Loader.php',
        'Dotenv\\Parser' => __DIR__ . '/..' . '/vlucas/phpdotenv/src/Parser.php',
        'Dotenv\\Validator' => __DIR__ . '/..' . '/vlucas/phpdotenv/src/Validator.php',
        'Framework\\Core\\Alias' => __DIR__ . '/../..' . '/framework/Core/Alias.php',
        'Framework\\Core\\Aliases\\App' => __DIR__ . '/../..' . '/framework/Core/Aliases/App.php',
        'Framework\\Core\\Aliases\\Request' => __DIR__ . '/../..' . '/framework/Core/Aliases/Request.php',
        'Framework\\Core\\Aliases\\Response' => __DIR__ . '/../..' . '/framework/Core/Aliases/Response.php',
        'Framework\\Core\\Aliases\\Routes' => __DIR__ . '/../..' . '/framework/Core/Aliases/Routes.php',
        'Framework\\Core\\Aliases\\Transport' => __DIR__ . '/../..' . '/framework/Core/Aliases/Transport.php',
        'Framework\\Core\\Container' => __DIR__ . '/../..' . '/framework/Core/Container.php',
        'Framework\\Core\\Kernel' => __DIR__ . '/../..' . '/framework/Core/Kernel.php',
        'Framework\\Helpers\\Helpers' => __DIR__ . '/../..' . '/framework/Helpers/Helpers.php',
        'Framework\\Routing\\Request' => __DIR__ . '/../..' . '/framework/Routing/Request.php',
        'Framework\\Routing\\Response' => __DIR__ . '/../..' . '/framework/Routing/Response.php',
        'Framework\\Routing\\Routes' => __DIR__ . '/../..' . '/framework/Routing/Routes.php',
        'Framework\\Routing\\Transport' => __DIR__ . '/../..' . '/framework/Routing/Transport.php',
        'Symfony\\Polyfill\\Ctype\\Ctype' => __DIR__ . '/..' . '/symfony/polyfill-ctype/Ctype.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInited47f589aa2a9a2c1c5dce02fd306d3e::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInited47f589aa2a9a2c1c5dce02fd306d3e::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInited47f589aa2a9a2c1c5dce02fd306d3e::$classMap;

        }, null, ClassLoader::class);
    }
}
