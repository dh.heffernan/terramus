<?php

namespace Framework\Routing;

use Framework\Routing\Request;
use Framework\Routing\Response;
use Framework\Routing\Routes;

class Router {

	protected $request;
	protected $response;
	protected $routes;

	public function __construct(Request $request, Response $response, Routes $routes)
	{
		$this->request 	= $request;
		$this->response = $response;
		$this->routes 	= $routes;
	}

	public function getRequest()
	{
		return $this->request->build();
	}

	public function handle()
	{
		$request = $this->getRequest();
		if ($request['method'] == 'GET') {
			$request['uri'] = substr($request['uri'], 1, strlen($request['uri']));
			if ($request['uri'] == '') {
				$segments = ['/'];
			} else {
				$segments = explode('/', $request['uri']);
			}
			foreach ($this->routes->routes[$request['method']] as $key => $value) {
				if (count($segments) == count($value['parameters'])) {
					$matched = [];
					foreach ($value['parameters'] as $k => $v) {
						if (!$this->isPlaceholder($v)) {
							if (in_array($v, $segments)) {
								array_push($matched, 1);
							} else {
								array_push($matched, 0);
							}
						} else {
							array_push($matched, 1);
						}
					}
					if (!in_array(0, $matched)) {
						$matches[$key] = $value;
					}
				}
			}
			switch (count($matches)) {
				case 0:
					return null;				
				case 1:
					reset($matches);
					$first_key = key($matches);
					$this->processMatched($matches[$first_key], $segments);
			}
		}
	}

	public function processMatched($route, $parameters)
	{
		$parameters = array_diff($parameters, $route['parameters']);
		$controller = explode('@', $route['action']);
		if (isset($route['middlewares']) && !is_null($route['middlewares'])) {
			$this->processMiddlewares($route['middlewares']);
		}
		$this->findController($controller);
		$this->findMethod($controller, $parameters);
	}

	protected function isPlaceholder($segment)
	{
		preg_match('/\{(.*?)\}/', $segment, $matches);
		return ($matches) ? 1 : 0;
	}

	public function sendError($code, $name, $message)
	{
		\Exceptions::routeError($code, array('name' => $name, 'message' => $message));
	}

	public function findController($controller)
	{
		if (!file_exists(\Kernel::getControllerPath() . $controller[0] . '.php')) {
			$this->sendError('400', \Kernel::getControllerPath() . $controller[0] . '.php', $controller[0] . ' controller not found');
		} else {
			\Kernel::makeService('App\\Controllers\\' . $controller[0]);
		}
	}

	public function findMethod($controller, $parameters)
	{
		if (!method_exists('App\\Controllers\\' . $controller[0], $controller[1])) {
			$this->sendError('400', 'App\\Controllers\\' . $controller[0] . '.php', $controller[1] .' method not found');
		} else {
			call_user_func_array(['App\\Controllers\\' . $controller[0], $controller[1]], $parameters);
		}
	}

	public function processMiddlewares($middlewares)
	{
		foreach ($middlewares as $key => $value) {
			if (!file_exists(\Kernel::getMiddlewaresPath() . $value . '.php')) {
				$this->sendError('401', \Kernel::getMiddlewaresPath() . $value . '.php', $value . ' middleware not found');
			} else {
				\Kernel::makeService('Framework\\Middlewares\\' . $value);
			}
		}
	}
		
}