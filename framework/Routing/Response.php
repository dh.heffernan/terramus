<?php

namespace Framework\Routing;

class Response {

	protected $code;
	protected $content;
	
	public function __construct($code = 200, $content = '')
	{
		$this->code 	= $code;
		$this->content 	= $content;
	}

	public function send()
	{
		echo $this->content;
	}
}