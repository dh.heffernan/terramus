<?php

namespace Framework\Core\Aliases;

use Framework\Core\Alias;

class Transport extends Alias
{
	protected static function serviceName()
    {
		return 'Framework\\Routing\\Transport';
	}
}